public with sharing class Demo_Bulk_API_Controller {

    @AuraEnabled
    public static String getRecordsToDownload(){
      String result = new String();
      String jobId = null;
      HttpResponse authResponse = Bp_NicPaymentCalls.createJobForBulkApi();
      if (authResponse.getStatusCode() == 200) {
        String resBody = authResponse.getBody();
        Map<String,Object> temp = (Map<String, Object>)(JSON.deserializeUntyped(authResponse.getbody()));
        jobId = String.valueOf(temp.get('id'));
        HttpResponse authResponse2 = Bp_NicPaymentCalls.getRecordsCallout(jobId); 
        if(authResponse2.getStatusCode() == 200){
            result = 'Success';
            //authResponse2 final result for further manipulation
        }else{
            result = 'Failed';
        }  
        
      }else{ 
          result = 'Failed';
      }
      return result;
      
    }    

    public Static HttpResponse createJobForBulkApi() {
        String baseUrl = 'Base Url of org';
        HttpRequest req = new HttpRequest();
        //The request body specifies the query to be performed.
        string responseparam = '{"operation": "query", "query": "SELECT Id, Name FROM Account"}';
        req.setBody(responseparam);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json');
        //Set Session Id
        req.setHeader('Authorization' ,'Bearer '+UserInfo.getOrganizationId()+''+UserInfo.getSessionId().SubString(15) );
        //Set Endpoint /services/data/vXX.X/jobs/query
        req.setEndpoint(baseUrl+'/services/data/v53.0/jobs/query');
        HttpResponse res = new HttpResponse();
        Http authHttp = new Http();
        res = authHttp.send(req);

        return res;
    }


    public Static HttpResponse getRecordsCallout(String queryJobId) {
        String baseUrl = 'Base Url of org';
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization' ,'Bearer '+UserInfo.getOrganizationId()+''+UserInfo.getSessionId().SubString(15) ); 
        //Set endpoint as /services/data/vXX.X/jobs/query/queryJobId/results 
        req.setEndpoint(baseUrl+'/services/data/v53.0/jobs/query/'+queryJobId+'/results');
        HttpResponse res = new HttpResponse();
        Http authHttp = new Http();
        res = authHttp.send(req);
        return res;
    }
}
